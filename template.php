<?php
// $Id: template.php,v 1.16.2.3 2010/05/11 09:41:22 gdzine Exp $


/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $settings = theme_get_settings('hurl');
  $vars['logo_img']         = $vars['logo'] ? theme('image', substr($vars['logo'], strlen(base_path())), t('Home'), t('Home')) : '';
  $vars['linked_logo_img']  = $vars['logo_img'] ? l($vars['logo_img'], '<front>', array('rel' => 'home', 'title' => t('Home'), 'html' => TRUE)) : '';

  if (!empty($settings['hurl_launch_date'])) {
    $vars['hurl_launch_date'] = $settings['hurl_launch_date'];
  }
  else {
    //$variables['hurl_launch_date'] = path_to_theme().'/head.jpg';
    $vars['hurl_launch_date'] = '12/12/2099';
  }

  $vars['hurl_launch_date_split'] = explode("/",$vars['hurl_launch_date']);
  
  if (!empty($settings['hurl_twitter_url'])) {
    $vars['hurl_twitter_url'] = $settings['hurl_twitter_url'];
  }
  else {
    //$variables['hurl_launch_date'] = path_to_theme().'/head.jpg';
    $vars['hurl_twitter_url'] = 'http://twitter.com/gdzinenet';
  }
  
  if (!empty($settings['hurl_facebook_url'])) {
    $vars['hurl_facebook_url'] = $settings['hurl_facebook_url'];
  }
  else {
    //$variables['hurl_launch_date'] = path_to_theme().'/head.jpg';
    $vars['hurl_facebook_url'] = 'http://facebook.com/page/gdzinenet';
  }
}


/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function phptemplate_get_ie_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/fix-ie.css" />';
  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/fix-ie-rtl.css";</style>';
  }

  return $iecss;
}

