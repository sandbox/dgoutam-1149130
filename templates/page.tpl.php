<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>

  <body>
    <div id="page" class="container-16">
        <div id="site-header" class="clear-block">
            <div id="branding" class="grid-16">
              <?php if ($linked_logo_img): ?>
                <div id="logo" class="grid-6 alpha"><?php print $linked_logo_img; ?></div>
              <?php endif; ?>
              <?php if ($site_slogan): print '<div id="slogan" class="grid-10 omega">'. $site_slogan .'</div>'; endif; ?>
            </div> <!--/#branding -->
	    </div> <!--/#site-header -->
	    
	    <div id="content-top" class="grid-16">
            <?php print $breadcrumb; ?>
            <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
            <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
            <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
            <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
            <?php print $help; ?>
	    </div> <!-- End of Content Top -->

	    <div class="clear-block"></div>
	    
	    <div id="content" class="grid-16">
            <div id="left" class="grid-4 alpha">
                <?php print $left; ?>
            </div>
            <div id="full-content" class="grid-12 omega">
                <?php print $content ?>
            </div>	    
	    </div>
	    
	    <div class="clear-block"></div>
	    
	    <?php if ($footer || $footer_message || $secondary_links): ?>
        <div id="footer" class="grid-16 alpha omega">
            <?php print $footer; ?>
            
            <?php if ($footer_message): ?>
                <div id="footer-message"><?php print $footer_message; ?></div>
            <?php endif; ?>

        </div><!-- End of Footer -->
        <?php endif; ?>
    
    </div> <!-- End of Page -->
  <?php print $closure ?>
  </body>
</html>      
