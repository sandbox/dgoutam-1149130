<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans|Lobster">
  <?php print $scripts; ?>
</head>

<body>
  <div id="page" class="container-16 clear-block">


    <div id="site-header" class="clear-block">
	    <div id="header-inner">
        <div id="branding" class="grid-16 clear-block">
          <?php if ($linked_logo_img): ?>
            <span id="logo" class="grid-1 alpha"><?php print $linked_logo_img; ?></span>
          <?php endif; ?>
        </div> <!--/#branding -->
      </div> <!--/#header-inner -->
	  </div> <!--/#site-header -->

    <div id="main" class="column grid-16">
  	  <div id="content-front">
  	    <div id="content-inner">
        <?php if ($site_slogan): ?>
            <div id="site-slogan" class="grid-16 alpha omega clear-block"><?php print $site_slogan; ?></div>
        <?php endif; ?>

        <?php if ($mission): ?>
            <div id="mission" class="grid-10 alpha omega clear-block">
              <?php print $mission; ?>
            </div>
        <?php endif; ?>
        <div class="clear-block"></div>
        
        <?php if (!_menu_site_is_offline()) : ?>
        
          <!-- Countdown dashboard start -->
        <div class="countdown">
		    <div id="countdown_dashboard">
			    <div class="dash weeks_dash">
				    <span class="dash_title">weeks</span>
				    <div class="digit">0</div>
				    <div class="digit">0</div>
			    </div>

			    <div class="dash days_dash">
				    <span class="dash_title">days</span>
				    <div class="digit">0</div>
				    <div class="digit">0</div>
			    </div>

			    <div class="dash hours_dash">
				    <span class="dash_title">hours</span>
				    <div class="digit">0</div>
				    <div class="digit">0</div>
			    </div>

			    <div class="dash minutes_dash">
				    <span class="dash_title">minutes</span>
				    <div class="digit">0</div>
				    <div class="digit">0</div>
			    </div>

			    <div class="dash seconds_dash">
				    <span class="dash_title">seconds</span>
				    <div class="digit">0</div>
				    <div class="digit">0</div>
			    </div>

		    </div>
		</div>
		<!-- Countdown dashboard end -->
		<?php endif; ?>
		
		<div class="clear-block"></div>
		
	    <div class="content-wrap grid-10">
            <?php print $content; ?>
        </div>
		</div> <!-- // #content-inner -->
	  </div> <!-- //#content -->
	</div> <!-- //end #main -->



    <div id="footer" class="alpha omega">
      <?php if ($footer): ?>
        <div id="footer_contact" class="region grid-8 clear-block">
    	    <div id="footer-inner">
                <?php print $footer; ?>
            </div>
        </div>
      <?php endif; ?>
      
      <?php if ($footer): ?>
        <div id="footer_social" class="region grid-8 clear-block">
    	    <div id="footer-inner">
                <?php print $footer; ?>
            </div>
        </div>
      <?php endif; ?>

      <?php if ($footer_message): ?>
        <div id="footer-message" class="grid-8">
          <?php print $footer_message; ?>
        </div>
      <?php endif; ?>
    </div> <!-- /#footer -->

  </div> <!-- /#page -->

<?php print $closure; ?>

<script language="javascript" type="text/javascript">
	jQuery(document).ready(function() {
		$('#countdown_dashboard').countDown({
			targetDate: {
				'day': 		<?php print_r($hurl_launch_date_split[1]);?>,
				'month': 	<?php print_r($hurl_launch_date_split[0]);?>,
				'year': 	<?php print_r($hurl_launch_date_split[2]);?>,
				'hour': 	11,
				'min': 		59,
				'sec': 		1
			}
		});
	});
</script>

</body>
</html>
