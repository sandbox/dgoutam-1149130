<?php
/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @param $subtheme_defaults
 *   Allow a subtheme to override the default values.
 * @return
 *   A form array.
 */
function hurl_settings($saved_settings, $subtheme_defaults = array()) {

  // Add the form's CSS
  drupal_add_css(drupal_get_path('theme', 'hurl') . '/jquery-ui/css/ui-lightness/jquery-ui-1.8.12.custom.css', 'theme');

  // Add javascript to show/hide optional settings
  drupal_add_js(drupal_get_path('theme', 'hurl') . '/jquery-ui/js/jquery-1.5.1.min.js', 'theme');
  drupal_add_js(drupal_get_path('theme', 'hurl') . '/jquery-ui/js/jquery-ui-1.8.12.custom.min.js', 'theme');
  drupal_add_js(drupal_get_path('theme', 'hurl') . '/js/hurl-theme-settings.js', 'theme');

  // Get the default values from the .info file.
  $defaults = hurl_theme_get_default_settings('hurl');

  // Allow a subtheme to override the default values.
  $defaults = array_merge($defaults, $subtheme_defaults);

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  /*
   * Create the form using Forms API
   */
  $form['hurl-div-opening'] = array(
    '#value'         => '<div id="hurl-settings">',
  );

  $form['social'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Hurl theme settings'),
    '#attributes'    => array('id' => 'hurl-settings'),
  );
  $form['social']['hurl_facebook_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook URL'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => $settings['hurl_facebook_url'],
    '#size'          => 40,
    '#maxlength'     => 100,
    '#prefix'        => '<div id="div-hurl-social-collapse">', // jquery hook to show/hide optional widgets
  );
 $form['social']['hurl_twitter_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Twitter URL'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => $settings['hurl_twitter_url'],
    '#size'          => 40,
    '#maxlength'     => 100,
    '#prefix'        => '<div id="div-hurl-social-collapse">', // jquery hook to show/hide optional widgets
  );

  $form['launch'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Launch date'),
    '#attributes'    => array('id' => 'hurl-launch'),
  );
 $form['launch']['hurl_launch_date'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Launch Date'),
    '#description'   => t('Enter date in dd/mm/yyyy'),
    '#default_value' => $settings['hurl_launch_date'],
    '#size'          => 12,
    '#maxlength'     => 10,
    '#prefix'        => '<div id="div-hurl-launch-collapse">', // jquery hook to show/hide optional widgets
  );

  $form['hurl-div-closing'] = array(
    '#value'         => '</div>',
  );

  // Return the form
  return $form;
}


/**
 * Return the theme settings' default values from the .info and save them into the database.
 *
 * @param $theme
 *   The name of theme.
 */
function hurl_theme_get_default_settings($theme) {
  $themes = list_themes();

  // Get the default values from the .info file.
  $defaults = !empty($themes[$theme]->info['settings']) ? $themes[$theme]->info['settings'] : array();

  if (!module_exists('domain_theme') && !empty($defaults)) {
    // Merge the defaults with the theme settings saved in the database.
    $settings = array_merge($defaults, variable_get('theme_'. $theme .'_settings', array()));
    // Save the settings back to the database.
    if (db_is_active()) {
      variable_set('theme_'. $theme .'_settings', $settings);
    }
    else {
      $GLOBALS['conf']['theme_'. $theme .'_settings'] = $settings;
    }
    // If the active theme has been loaded, force refresh of Drupal internals.
    if (!empty($GLOBALS['theme_key'])) {
      theme_get_setting('', TRUE);
    }
  }

  // Return the default settings.
  return $defaults;
}

